package pokeleague;
import java.util.Scanner;
/**
 *
 * @author Carlos Páez - Mauricio Monroy
 */
public class PokeLeague {
    
    public static void main(String[] args) {
        
        //boolean winner =true, loser= true;
        
        Core a = new Core();
        
        Scanner scan = new Scanner(System.in);
        System.out.println("Ingresa la cantidad de entrenadores");
        int n = scan.nextInt();
        String [] trainers = new String[n];
        
        for (int i = 0; i < n; i++) {
            System.out.println("Ingresa el nombre del ( "+(i+1)+" ) entrenador");
            String val = scan.next();
            trainers[i] = val;
            System.out.println("Escoja sus pokemon: ");
            //a.prueba();
            a.seleccionPokemon();
        }

        System.out.println("Los entrenadores son: ");
        for (int i = 0; i < n; i++) {
            System.out.println("Entrenador numero "+ (i+1) +" :"+ trainers[i]);
        }
        
        for (int i = 0; i < n; i++) {
        System.out.println("El entrenador - " + trainers[i] + 
                " tiene el pokemon - " + a.namepoke + " | Vida " + a.getHp() + "   | Ataque " + a.getAp()
                + "   | Velocidad " + a.getAs() + "   | Tipo " + a.tipo);
        }
        
//Lógica de batalla
        
        //do{
            
        //}while(winner && loser);
        
    }
}