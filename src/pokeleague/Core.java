package pokeleague;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Carlos Páez
 */
public class Core {
    
    // Hp = Health Power, Ap = Attack Power, cl = clase o tipo, As = Attack Speed
    private int hp, ap, cl, as;
    public String namepoke, tipo;
    private Random shuffle = new Random(System.nanoTime());
    Scanner scan = new Scanner(System.in);

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getAp() {
        return ap;
    }

    public void setAp(int ap) {
        this.ap = ap;
    }

    public int getCl() {
        return cl;
    }

    public void setCl(int cl) {
        this.cl = cl;
    }

    public int getAs() {
        return as;
    }

    public void setAs(int as) {
        this.as = as;
    }
    
    // método para ataques
    
    public int ataquePokemon(){
        int apvalue = shuffle.nextInt(50);
        return apvalue;
    }
    
    // método para daños recibidos
    
    public void danio(int hp){
        this.hp-=hp;
    }
    
    public void pruebaSeleccion(){
               
            System.out.println("Selecciona pokemon:\n");
            System.out.println("1. Pikachu          | 2. Charmander          | 3. Ditto");
            
            int opcion = scan.nextInt();
            
            for(int i=0; i<2; i++){
            if (opcion == 1){
                namepoke="Pikachu";
                this.setHp(100);
                this.setAs(shuffle.nextInt(50));
                this.setAp(10);
                tipo="Eléctrico";
                
            }if(opcion == 2){
                namepoke="Charmander";
                this.setHp(100);
                this.setAs(shuffle.nextInt(50));
                this.setAp(shuffle.nextInt(20));
                tipo="Fuego";
            }else{
                namepoke="Ditto";
                this.setHp(100);
                this.setAs(shuffle.nextInt(50));
                this.setAp(shuffle.nextInt(20));
                tipo="Normal";
            }
        }
    }
    
    //
    public void seleccionPokemon(){
    
        int pokemon[]= new int[3];
        
        for (int i = 0; i <= 2; i++) {
            System.out.println("Selecciona el " + (i+1) + " pokemon\n");
            System.out.print("1. Pikachu          | 2. Charmander          | 3. Ditto - ");
            int opcion = scan.nextInt();
            
            if (opcion == 1){
                namepoke="Pikachu";
                this.setHp(100);
                this.setAs(shuffle.nextInt(50));
                this.setAp(10);
                tipo="Eléctrico";
                System.out.println(namepoke + " | Vida " + getHp() + "   | Ataque " + getAp()
                + "   | Velocidad " + getAs() + "   | Tipo " + tipo);
                
            }if(opcion == 2){
                namepoke="Charmander";
                this.setHp(100);
                this.setAs(shuffle.nextInt(50));
                this.setAp(shuffle.nextInt(20));
                tipo="Fuego";
                System.out.println(namepoke + " | Vida " + getHp() + "   | Ataque " + getAp()
                + "   | Velocidad " + getAs() + "   | Tipo " + tipo);
            }if(opcion ==3){
                namepoke="Ditto";
                this.setHp(100);
                this.setAs(shuffle.nextInt(50));
                this.setAp(shuffle.nextInt(20));
                tipo="Normal";
                System.out.println(namepoke + " | Vida " + getHp() + "   | Ataque " + getAp()
                + "   | Velocidad " + getAs() + "   | Tipo " + tipo);
            }
            pokemon[i] = opcion;
        }
        
//        for (int i = 0; i <=2; i++) {
//        System.out.println(namepoke + " | Vida " + getHp() + "   | Ataque " + getAp()
//                + "   | Velocidad " + getAs() + "   | Tipo " + tipo);
//        }
        
    }
    
}